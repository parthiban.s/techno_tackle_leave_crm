import React from 'react';
import { StyleSheet, Text, View, DrawerLayoutAndroid ,Picker} from 'react-native';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import { TextInput } from 'react-native-gesture-handler';
import { Dropdown } from 'react-native-material-dropdown';
import TimePicker from "react-native-24h-timepicker";



export default class App extends React.Component {

  constructor(props) {
    super(props);
  }
  state = {user: '',
   start_time: "",
   to_time: "",
}
   updateUser = (user) => {
      this.setState({ user: user })
   }


  start_time_onCancel() {
    this.TimePicker.close();
  }
 
  start_time_onConfirm(hour, minute) {
    this.setState({ start_time: `${hour}:${minute}` });
    this.TimePicker.close();
  }

  to_time_onCancel() {
    this.TimePicker.close();
  }
 
 to_time_onConfirm(hour, minute) {
    this.setState({ to_time: `${hour}:${minute}` });
    this.TimePicker.close();
  }



  async componentDidMount() {
    await Font.loadAsync({
      'poppins': require('./assets/fonts/Poppins-Black.otf'),
      'poppinsItalic': require('./assets/fonts/Poppins-BlackItalic.otf'),

    });

    this.setState({ assetsLoaded: true });
  }
  render() {
  
    var navigationView = (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <Text style={{ margin: 10, fontSize: 15, textAlign: 'left' }}>I'm in the Drawer!</Text>
      </View>
    );
    return (
      <DrawerLayoutAndroid
        drawerWidth={300}
        drawerPosition={DrawerLayoutAndroid.positions.Left}
        renderNavigationView={() => navigationView}>
        <View style={{ flex: 1, }}>
          <Text style={styles.center} >Leave Request</Text>
          <Text style={styles.normal} >Fields with * are required.</Text>
          <Text style={styles.input} >Leave Type.</Text>
          <Picker selectedValue = {this.state.user} onValueChange = {this.updateUser }  style={styles.card} >
               <Picker.Item label = "- Select -" value = "- Select -" />
               <Picker.Item label = "Full Day" value = "Full Day" />
               <Picker.Item label = "Half Day" value = "Half Day" />
               <Picker.Item label = "Two (2) hrs" value = "Two (2) hrs" />
               <Picker.Item label = "Compensation Full Day" value = "Compensation Full Day" />
               <Picker.Item label = "Compensation Half Day" value = "Compensation Half Day" />
               <Picker.Item label = "Work From Home" value = "Work From Home" />
            </Picker>
            <View  style={ {paddingTop:10}}  >
              <Text  style={styles.input}  >From (hrs)     </Text>
              <TimePicker
                  ref={ref => {
                    this.TimePicker = ref;
                  }}
                  onCancel={() => this.start_time_onCancel()}
                  onConfirm={(hour, minute) => this.start_time_onConfirm(hour, minute)}
                />
               <Text  style={styles.cardtext}  onPress={() => this.TimePicker.open()} >{this.state.start_time}</Text> 
            </View>

            <View  style={ {paddingTop:10}}  >
              <Text  style={styles.input}  >To  (hrs)     </Text>
              <TimePicker
                  ref={refer => {
                    this.TimePicker = refer;
                  }}
                  onCancel={() => this.to_time_onCancel()}
                  onConfirm={(hour, minute) => this.to_time_onConfirm(hour, minute)}
                />
               <Text  style={styles.cardtext}   onPress={() => this.TimePicker.open()} >{this.state.to_time}</Text> 
            </View>

          </View>
      </DrawerLayoutAndroid>
    );
  }


}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  center: {
    marginTop: 80,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    fontSize: 20,
    color: 'black',

  },
  card: {
    marginTop: 10,
    width: '90%',
    borderRadius: 10,
    borderColor: "#ccc",
    backgroundColor: '#fff',
    elevation: 6,
    alignSelf: 'flex-start',
    marginStart: 10,
    marginEnd: 20,
    paddingStart:10,
  },
  normal: {
    marginTop: 20,
    paddingTop: 10,
    backgroundColor: '#fff',
    fontSize: 12,
    color: 'grey',
    alignSelf: 'flex-start',
    marginStart: 10,
  },
  input: {
    marginTop: 10,
    fontSize: 14,
    color: 'black',
    alignSelf: "flex-start",
    marginStart: 10,
  },

  cardtext: {
    marginTop: 10,
    width: '90%',
    borderRadius: 10,
    borderColor: "#ccc",
    backgroundColor: '#fff',
    elevation: 6,
    alignSelf: 'flex-start',
    marginStart: 10,
    marginEnd: 20,
    padding:10,
    fontSize:14,
  },

});
